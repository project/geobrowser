<?php

$geobrowser_path = drupal_get_path('module', 'geobrowser');

	function geobrowser_intial_info_window_path(){
		if (variable_get(geobrowser_initial_page,'') == '<none>') return(drupal_get_path('module', 'geobrowser')."/loading.html");
	  else return(base_path().'?q='.variable_get(geobrowser_initial_page,''));
	}

  function geobrowser_java_vocab_titles(){    
    $vocabs = geobrowser_get_vocabs();
    
	  $output = "var vocab_titles = new Array(); ";
	  foreach ($vocabs as $vocab_id) {
			if ($vocab_id){
		    $get_vid_titles = db_query("SELECT name from {vocabulary} where vid = " . $vocab_id);
		    $title_object = db_fetch_object($get_vid_titles);
		    $title = $title_object->name;
		    $output .= "vocab_titles[$vocab_id] = '".$title."'; ";
		  }
	  }
    
    $i=0;
    foreach ($vocabs as $vocab_id) {
      $output .= "document.getElementById('tree_holder').innerHTML += \"<div id='tree_menu$i' style='display:none;'></div>\"; ";
      $i++;
    }
    
    $i=0;
    foreach ($vocabs as $vocab_id) {
      $output .= "build_tree($i); ";
      $i++;
    }
     
  return $output;
  }
  
  function geobrowser_base_maps(){
    $output = 'map.getMapTypes().length = 0; META_TILING = true;';
    $default_map;
    $base_map_results = db_query("select * from {geobrowser_basemaps} WHERE enabled = 1");
    while ($base_map = db_fetch_object($base_map_results)) {
      if ($base_map->id == 1) $output.= "map.addMapType(G_NORMAL_MAP);";
      if ($base_map->id == 2) $output.= "map.addMapType(G_SATELLITE_MAP);";
      if ($base_map->id == 3) $output.= "map.addMapType(G_HYBRID_MAP);";
      
      if ($base_map->id >= 4) {
        if ($base_map->type == "WMS"){
	        $output .= "
	          var layerconfig".$base_map->id." = new GTileLayer(new GCopyrightCollection(''),1,17);
	          layerconfig".$base_map->id.".myLayers='".$base_map->layer."';
	          layerconfig".$base_map->id.".myBaseURL='".$base_map->address."';
	          layerconfig".$base_map->id.".getTileUrl=CustomGetTileUrl;
	          layerconfig".$base_map->id.".myOpacity=".$base_map->opacy.";
	          layerconfig".$base_map->id.".getOpacity=customOpacity;
	          var layer".$base_map->id."=[G_SATELLITE_MAP.getTileLayers()[0],layerconfig".$base_map->id."];
	          var custommap".$base_map->id." = new GMapType(layer".$base_map->id.", G_SATELLITE_MAP.getProjection(), '".$base_map->display_name."', G_SATELLITE_MAP);
	          map.addMapType(custommap".$base_map->id.");
	        ";
      	}
      	if ($base_map->type == "tilecache"){
      	  $output .= "
      	  	var copyCollection".$base_map->id." = new GCopyrightCollection('Chart');
						var copyright".$base_map->id." = new GCopyright(1, new GLatLngBounds(new GLatLng(-90, -180), new GLatLng(90, 180)), 0, '');
						copyCollection".$base_map->id.".addCopyright(copyright".$base_map->id.");

						var tilelayer".$base_map->id." = [new GTileLayer(copyCollection".$base_map->id.", ".$base_map->tile_min.", ".$base_map->tile_max.")];
						tilelayer".$base_map->id."[0].getTileUrl = CustomGetTileUrl".$base_map->id.";

						function CustomGetTileUrl".$base_map->id."(a,b) {
							return '".$base_map->tile_path."'+b+'_'+a.x+'_'+a.y+'.png';
						}

						var custommap".$base_map->id." = new GMapType(tilelayer".$base_map->id.", new GMercatorProjection(".($base_map->tile_max + 1)."), '".$base_map->display_name."');
						map.addMapType(custommap".$base_map->id.");
      		
      		";
      	}
      }
      
      
      if (variable_get('geobrowser_default_basemap', 1) == $base_map->id) $default_map = $base_map->id;
    }
    $output .= "map.setCenter(new GLatLng(begin_lat,begin_lon), begin_zoom);";
    if ($default_map == 1) $output.= "map.setMapType(G_NORMAL_MAP);";
    if ($default_map == 2) $output.= "map.setMapType(G_SATELLITE_MAP);";
    if ($default_map == 3) $output.= "map.setMapType(G_HYBRID_MAP);";
    if ($default_map >= 4) $output.= "map.setMapType(custommap".$default_map.");";
    return $output;
  }
  
  function geobrowser_certain_cats(){
  	if ($_GET['categories']){
  		return "check_categories('".$_GET['categories']."');";
  	}
  }
  
  function geobrowser_additional_css(){
  	$output = "";
  	if ($_GET['additional_css']){
  		$additional_css_array = explode(",",$_GET['additional_css']);
  		foreach ($additional_css_array as $additional_css){
  			$output .= "<link rel='STYLESHEET' type='text/css' href='".drupal_get_path('module', 'geobrowser')."/".$additional_css."'/>";
  		}
  		return $output;
  	}
  }
  
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <title><?php print(variable_get('geobrowser_title','Geobrowser')); ?></title>

  <!-- Loading scripts for our side check-box menu -->
  <script src="<?php print $geobrowser_path; ?>/controls/tree/js/dhtmlXCommon.js" type="text/javascript"></script>
  <script src="<?php print $geobrowser_path; ?>/controls/tree/js/dhtmlXTree.js" type="text/javascript"></script>
  <script src="<?php print $geobrowser_path; ?>/wms-gs.js" type="text/javascript"></script>
  <link rel="STYLESHEET" type="text/css" href="<?php print $geobrowser_path; ?>/controls/tree/css/dhtmlXTree.css"/>
  <link rel="STYLESHEET" type="text/css" href="<?php print $geobrowser_path; ?>/tabs.css"/>
  <link rel="STYLESHEET" type="text/css" href="<?php print $geobrowser_path; ?>/geobrowser.css"/>
	<?php print geobrowser_additional_css(); ?>
  <!--[if IE 6]><style type="text/css">.sidebar { background-color:white } </style><![endif]-->
  
  <!-- Script for managing page layout -->
  <script>
  window.onresize = resize_layout;
  function resize_layout(){
        document.getElementById('map').style.height = ((document.documentElement.clientHeight))+ 'px';
        document.getElementById('info_window').style.height = ((document.documentElement.clientHeight)*.7)+ 'px';
  };
  </script>


<!-- Loading the google map core -->
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php print(variable_get('geobrowser_gmap_key','')); ?>" type="text/javascript"></script>
<script src="<?php print $geobrowser_path; ?>/controls/googleMapTooltips.js" type="text/javascript"></script>


</head>

<body onunload="GUnload()">

    <div id="control_div">
    <img id="view-refresh" src="<?php print $geobrowser_path; ?>/img/view-refresh.png" OnClick="map_redraw();" onmouseover="set_message(refresh_text)" onmouseout="set_message()"/>
    <img id="zoom-out" src="<?php print $geobrowser_path; ?>/img/zoom-out.png" OnClick="map.zoomOut();" onmouseover="set_message(zoom_out_text)" onmouseout="set_message()"/>
    <img id="zoom-in" src="<?php print $geobrowser_path; ?>/img/zoom-in.png" OnClick="map.zoomIn();" onmouseover="set_message(zoom_in_text)" onmouseout="set_message()"/>
    </div>
    
<!-- Our Main Map Window -->
<div id="map"></div>

<div id="sidebar-background" class="sidebar"></div>
<div class="sidebar" id="sidebar-foreground">
  <h2 id="geobrowser_title"><?php print(variable_get('geobrowser_title','Geobrowser')); ?></h2>  
  <?php eval("?> ".variable_get('geobrowser_additional_html','')); ?>
  
  <!-- Our Layer / Feature Tree -->
  <ul id="tabnav">
  </ul>
  
  <div id='tree_holder'>
  </div>
  
  <div id="message_center"></div>
  
</div>

<div id="info_window" style="<?php if (variable_get(geobrowser_initial_page,'') == '<none>') print(' display:none; '); ?>">
<img src="<?php print $geobrowser_path; ?>/img/close.png" id="close_button" onClick="hidediv('info_window')"/>
  <iframe id="info_frame" name="info_frame_name" src="<?php print geobrowser_intial_info_window_path(); ?>"></iframe>
</div>

<script  type="text/javascript">resize_layout();</script>

<!-- Our Main Script -->
<script type="text/javascript">
//<![CDATA[
  
  function var_dump(obj) {
     if(typeof obj == "object") {
        return "Type: "+typeof(obj)+((obj.constructor) ? "\nConstructor: "+obj.constructor : "")+"\nValue: " + obj;
     } else {
        return "Type: "+typeof(obj)+"\nValue: "+obj;
     }
  }; //var_dump
  
  function set_message(message){
  	if (typeof message=="undefined") document.getElementById("message_center").innerHTML = '';
    else document.getElementById("message_center").innerHTML = message;
  }
  
  function hidediv(id) {
    //safe function to hide an element with a specified id
    if (document.getElementById) { // DOM3 = IE5, NS6
      document.getElementById(id).style.display = 'none';
    }
    else {
      if (document.layers) { // Netscape 4
        document.id.display = 'none';
      }
      else { // IE 4
        document.all.id.style.display = 'none';
      }
    }
    document.getElementById('info_frame').src = "<?php print $geobrowser_path; ?>/loading.html";
  }; //hidediv
  
  function showdiv(id) {
    //safe function to show an element with a specified id
        
    if (document.getElementById) { // DOM3 = IE5, NS6
      document.getElementById(id).style.display = 'block';
    }
    else {
      if (document.layers) { // Netscape 4
        document.id.display = 'block';
      }
      else { // IE 4
        document.all.id.style.display = 'block';
      }
    }
  }; //showdiv
  
  
  function start_map_move(){
    start_lat_min = map.getBounds().getSouthWest().lat();
    start_lat_max = map.getBounds().getNorthEast().lat();
    start_lon_min = map.getBounds().getSouthWest().lng();
    start_lon_max = map.getBounds().getNorthEast().lng();
  }; //start_map_move
  
  function end_map_move(){
    dump_outside_markers();
    if ((map.getBounds().getNorthWest().lat > start_lat_max) && (map.getBounds().getNorthWest().lng > start_lon_min)) {
      load_markers(map.getBounds().getNorthWest().lat, start_lat_max, map.getBounds().getNorthEast().lng, map.getBounds().getNorthWest().lng);
      load_markers(start_lat_max, map.getBounds().getSouthWest().lat, map.getBounds().getNorthEast().lng, start_lat_max);
      }
  }; //end_map_move
  
  
  function initial_loader(){
    j=0;
    while (j<tree.length) {
      var layers_string = tree[j].getAllChecked(); //Get all layers. We use the getAllChecked command, which is regretable, as there is no getAll function
      var all_layers = layers_string.split(','); 
      for (var i=0; i<all_layers.length; i++) { //Load up all our Icons.
        var tid = all_layers[i];
        marker_icons[tid] = new GIcon();
        marker_icons[tid].image = tree[j].imPath + tree[j].getItemImage(tid, 0) ;
        marker_icons[tid].iconAnchor = new GPoint((icon_width/2), (icon_height/2));
        marker_icons[tid].iconSize = new GSize(icon_width, icon_height);
      }
      j++;
    }
    <?php print geobrowser_certain_cats(); ?>
     change_tree(0);
    
  }; //initial_loader
  
  function node_clicked(marker, point){
  	//try{
  		if (marker instanceof GMarker){
  			document.getElementById('info_frame').src = base_path + "?q=node/" + marker.nid;
	    	showdiv('info_window');
  		}
  		else{
  			if (new_marker_mode){
  				document.getElementById('info_frame').src = base_path + "?q=node/add/" + new_marker_node_type;
	    		showdiv('info_window');
	    		set_info_window_lat_lon(point.lat(), point.lng(), 0);
  			}
  		}
  		new_marker_mode = false;
	    set_message();
	  //}catch(err){}
  }; //node_clicked
  

  
  function set_info_window_lat_lon(lat, lon,count){
  	try{
  		frames['info_frame_name'].document.getElementById('edit-locations-0-locpick-user-latitude').value = lat;
  		frames['info_frame_name'].document.getElementById('edit-locations-0-locpick-user-longitude').value = lon;
  	}
  	catch(err){
  		if (count <= 20){
  			count = count + 1;
  			setTimeout("set_info_window_lat_lon("+lat+","+lon+","+count+")",500);
  		}
  	}
  }
  
  function map_redraw(){
    map.clearOverlays(); // clear the markers off the screen
    marker_set.length = 0; //reset the marker array
    map_load_markers(); //reload the markers
  }; //map_redraw
  
  function map_zoomed(oldLevel, newLevel){
    if (oldLevel < newLevel) map_load_markers(); else map_redraw();
  }; //map_zoomed
  
  function catagory_clicked(clicked_id){
	  uncheck_tree();
    tree[current_tree].setSubChecked(clicked_id,1);
    map_redraw();
  }; //catagory_clicked
  
  function check_categories(checked_cats_csv){
  	uncheck_tree();
  	var checked_cats = checked_cats_csv.split(',');
  	for (var i=0; i<checked_cats.length; i++) {
      tree[current_tree].setSubChecked(checked_cats[i],1);
    }
  }
  
  function uncheck_tree() {
  	var layers_string = tree[current_tree].getAllChecked();
    var all_layers = layers_string.split(','); 
    for (var i=0; i<all_layers.length; i++) {
      tree[current_tree].setCheck(all_layers[i],0);
    }
  }
  
  function map_load_markers(){ // map_load_markers is called whenever we move the map or we click on or off some layers on the tree. Via a GXmlHttp request we get a list of points that match our layers and fit within our map window.
    var layers_string = "";
    j=0;
    while(j<tree.length) {
    	var each_layer_string = tree[j].getAllChecked();
      if (each_layer_string == '') each_layer_string = "0";
      layers_string += "&layer" + j + "=";
      layers_string += each_layer_string;
      j++;
    }
    
    var lat_min = map.getBounds().getSouthWest().lat();
    var lat_max = map.getBounds().getNorthEast().lat();
    var lon_min = map.getBounds().getSouthWest().lng();
    var lon_max = map.getBounds().getNorthEast().lng();
    
    var url_to_open = base_path + "?q=geobrowser_xml_controller&function=map_node" + layers_string + "&current_tree=" + current_tree +"&lat_min=" + lat_min + "&lat_max=" + lat_max + "&lon_min=" + lon_min + "&lon_max=" + lon_max;
    
    function remove_markers(element, keyVar, array) { //A function for clearing all the markers outside our view
          if ((marker_set[keyVar].getPoint().lat() > lat_max) || (marker_set[keyVar].getPoint().lat() < lat_min) || (marker_set[keyVar].getPoint().lng() > lon_max) || (marker_set[keyVar].getPoint().lng() < lon_min)) {
          map.removeOverlay(marker_set[keyVar]);
          delete marker_set[keyVar];
          }
    }
  
    //Javascript 1.5 implements forEach, which is faster. So we try that first. If the user does not have 1.5 then we try the older, slower method
    try {
    	marker_set.forEach(remove_markers)
    } catch(error) {
    	for (i in marker_set) {
    		remove_markers(marker_set[i], i, marker_set);
    	}
    }
    
    GDownloadUrl(url_to_open, function(data, responseCode){
      var xml = GXml.parse(data);
      var new_markers = xml.documentElement.getElementsByTagName("marker");
      for (var y = 0; y < new_markers.length; y++) { //for each marker found in the xml file
        
        //get the nid from the xml
        var nid = new_markers[y].getAttribute("nid");
  
        if (!marker_set[nid]) { //if marker does not yet exist, then load it up!
  
          //grab the lat and long info and parse into a format google maps can understand
          var point = new GLatLng(parseFloat(new_markers[y].getAttribute("lat")),parseFloat(new_markers[y].getAttribute("lng")));
          
          //grab the rest of the attributes and load into variables
          var name = new_markers[y].getAttribute("name");
          var layer = new_markers[y].getAttribute("layer");
  
          //Create the GMarker and load the variables into it
          marker_set[nid] = new GMarker(point, {icon: marker_icons[layer]<?php if (variable_get('geobrowser_draggable_icons', 0) == 1): ?>, draggable: true<?php endif;?>});
          marker_set[nid].nid = nid;
          marker_set[nid].name = name;
          marker_set[nid].layer = layer;
          
          setupMarkerForTooltip(map, marker_set[nid], marker_set[nid].name ,nid); // turn on tooltip support 
          setShowTooltipOnMouseover(marker_set[nid]);
          map.addOverlay(marker_set[nid]);
          
          <?php if (variable_get('geobrowser_draggable_icons', 0) == 1): ?>
	          marker_set[nid].enableDragging();
						GEvent.addListener(marker_set[nid], "dragend", marker_dragged);
					<?php endif;?>
          
        }
      }
    });
  }; //map_load_markers
  
  
  function build_tree(i){
    tree[i]=new dhtmlXTreeObject('tree_menu' + i,"100%","100%",0);
    tree[i].enableCheckBoxes(true);
    tree[i].enableThreeStateCheckboxes(true);
    tree[i].imPath = base_path + files_path + "/";
    
    tree[i].setOnClickHandler(function Clicking(clicked_id){catagory_clicked(clicked_id);});
    tree[i].setOnCheckHandler(function(){if (map) {map_redraw()}});
    
    try {
    tree[i].loadXML(base_path + "?q=geobrowser_xml_controller&function=build_tree&vid=" + vocabs[i], function (){
        if (i == (vocabs.length -1)) {
        	setTimeout("initial_loader()",500);
        }
      });//load root level from xml
    } catch(e) {
    	alert(e)
    }
      
    //Setting up the titles and tabs.
    if (vocabs.length > 1){
    	var vocab_title = vocab_titles[vocabs[i]];
    	document.getElementById("tabnav").innerHTML += "<li class='tab1' id='tab" + i + "'><a href='#' onClick='change_tree(" + i + ");'>" + vocab_title + "</a></li>";
  	}
  }; //build_tree
  
  
  function change_tree(i){
    var j = 0;
    while (j < tree.length) {
      var id = 'tree_menu' + j;
      document.getElementById(id).style.display = 'none';
      j++;
    }
    var id = 'tree_menu' + i;
    document.getElementById(id).style.display = 'block';
    current_tree = i;
    map_redraw();
  }; //change_tree
  
  
  function marker_dragged() {
				   document.getElementById('info_frame').src = base_path + "?q=node/" + this.nid + "/edit";
				   showdiv('info_window');
				   var latlon = this.getPoint();
					 set_info_window_lat_lon(latlon.lat(), latlon.lng(), 0);
  }
  
  function new_marker(nodetype){
  	new_marker_mode = true;
  	set_message(add_marker_text);
  	new_marker_node_type = nodetype;
  }
  			 
  
  //Set-up our global variables
  var marker_set = new Array();    //load up our array of marker data
  var marker_icons = new Array();  //load up all our marker icons to be displayed on the map.
  var new_marker_mode = false;
  var new_marler_node_type;
  var tree = new Array();
  var view_lat_min;
  var view_lat_max;
  var view_lon_min;
  var view_lon_max;
  var start_lat_min;
  var start_lat_max;
  var start_lon_min;
  var start_lon_max;
  var current_tree = 0;
  var base_path = "<?php print(base_path()); ?>";
  var files_path = "<?php print(file_directory_path()); ?>";
  var icon_height = "<?php print(variable_get('taxonomy_image_height',18)); ?>";
  var icon_width = "<?php print(variable_get('taxonomy_image_width',18)); ?>";
  var gmap_key = "<?php print(variable_get('geobrowser_gmap_key','')); ?>";
  var begin_lat = "<?php print(variable_get('geobrowser_begin_lat', '0')); ?>";
  var begin_lon = "<?php print(variable_get('geobrowser_begin_lon', '0')); ?>";
  var begin_zoom = <?php print(variable_get('geobrowser_begin_zoom', 7)); ?>;
  var zoom_in_text = "<?php print(t('Zoom In')); ?>";
  var zoom_out_text = "<?php print(t('Zoom Out')); ?>";
  var refresh_text = "<?php print(t('Reload Markers')); ?>";
  var add_marker_text = "<?php print(t('Click on the map to add a new marker')); ?>";
  var geobrowser_path = "<?php print($geobrowser_path); ?>";

  
  //Set-up our global vocabulary variables
  
  var vocabs = new Array();
  
  <?php
    $vocabs = geobrowser_get_vocabs();
    $i=0;
    foreach($vocabs as $vocab_id){
    	print ("vocabs[$i] = ".$vocab_id."; ");
    	$i++;
    }
    
    print("var vocab_vid = '");
    foreach($vocabs as $vocab_id){
    	print ($vocab_id.",");
    }
    print("';");
  ?>
  
  //Initialize the whole shibang
  
  <?php print geobrowser_java_vocab_titles(); ?>
  
  //Load up our google map
  var map = new GMap2(document.getElementById("map"));
  <?php print geobrowser_base_maps(); ?>
  
  map.addControl(new GMapTypeControl( ),new GControlPosition(G_ANCHOR_TOP_LEFT,new GSize(7,7)));
  
  // ====== set up marker mouseover tooltip div ======
  var tooltip = document.createElement("div");
  map.getPane(G_MAP_FLOAT_PANE).appendChild(tooltip);
  tooltip.style.backgroundColor="white";
  tooltip.style.border="thin dotted blue";
  tooltip.style.width="150px";
  tooltip.style.visibility="hidden";
  
  //Add all our event listeners for the map
  GEvent.addListener(map, "click", node_clicked);
  GEvent.addListener(map, "moveend", map_load_markers);  // listning for the user panning. Triggers the map_moved function
  GEvent.addListener(map, "zoomend", map_zoomed); // map is redrawn on a zoom
//]]>
</script>

</body>

</html>