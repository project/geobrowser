  CREATE TABLE {geobrowser_basemaps} (
  `id` int(10) NOT NULL auto_increment,
  `name` longtext NOT NULL,
  `address` longtext NOT NULL,
  `layer` longtext NOT NULL,
  `opacy` decimal(10,2) NOT NULL,
  `enabled` int(1) default NULL,
  `display_name` longtext,
  PRIMARY KEY  (`id`)
)

INSERT INTO {geobrowser_basemaps} VALUES ('1', 'GMap Road', '', '', '1.00', '1', '');
INSERT INTO {geobrowser_basemaps} VALUES ('2', 'GMap Satellite', '', '', '1.00', '1', '');
INSERT INTO {geobrowser_basemaps} VALUES ('3', 'GMap Hybrid', '', '', '1.00', '1', '');
INSERT INTO {geobrowser_basemaps} VALUES ('4', 'WMS 1', '', '', '0.75', '0', '');
INSERT INTO {geobrowser_basemaps} VALUES ('5', 'WMS 2', '', '', '0.75', '0', '');
INSERT INTO {geobrowser_basemaps} VALUES ('6', 'WMS 3', '', '', '0.75', '0', '');
INSERT INTO {geobrowser_basemaps} VALUES ('7', 'WMS 4', '', '', '0.75', '0', '');