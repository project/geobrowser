To Install:

1) Download, install, and *configure* dependancies: location, fivestar, taxonomy_image
2) Download, install, and enable the Geobrowser module.
2.a) Upon install the module will copy geobrowser/img/files_to_copy/* to your files directory - but sometimes this doesn't work. If it looks like it didn't copy, copy contents of this folder over manually.
3) Create a vocabulary (Meta-category) and assign it to spatially enabled nodes.
4) Go to /admin/settings/geobrowser and configure geobrowser - make sure you properly set-up the vocabularies.
5) Set up your icons using the category_image module
6) Create some nodes and assign them lat/lon, assign them to a category within the vocabulary you set-up in step 3
7) Go to /geobrowser for some awsomeness

Adding markers within geobrowser:
	Add this to "Additional HTML/PHP" under /admin/settings/geobrowser
	
		<a href="#" OnClick="new_marker('**nodetypehere**')">Add New Marker</a>
	
	Where **nodetypehere** is the name of the node type you wish to add ('page', 'story' etc.)
	This will prompt the user to click on the map - when they do a new node window will pop-up with the lat/lon filled in for where they clicked.


Custom CSS:

	Query the map like this to load cystom css files. (custom css files should be located in the geobrowser module directory)
	
		/geobrowser?custom_css=geobrowser_no_sidebar.css			OR		/?q=geobrowser&custom_css=geobrowser_no_sidebar.css


Loading only certain categories:

	Query the map like this to load only certain categories:
	
		/geobrowser?categories=2,3,4,5			OR		/?q=geobrowser&categories=2,3,4,5


Additional Basemaps:
	There are two types of basemaps you can add to the geobrowser - WMS and TileCache. Adding new basemaps is considered an ADVANCED topic.
	
	WMS: 
		Pretty self-explanatory if you are familiar with WMS. Doesn't work 100% so be gentle. Geoserver can serve WFS and vectors as WMS.
	
	TileCache:
		This allows you to add giant images you might have as a basemap. To do this you must chop and scale your image so that it fits within google's requirements. See http://www.google.com/apis/maps/documentation/overlays.html#Custom_Map_Types for more info. Don't worry about the code - geobrowser will generate it for you - but you do need to worry about properly tiling your image.
		
		Included in the /geobrowser/tools directory are three tools that will help you do tiling. The most important is "tilescript.js" which is a script for Adobe Photoshop that will automatically cut tiles. To use it, edit tilescript.js and configure to your liking, then run it in photoshop. It works in CS3 - I dont know about any other version.
		
		It's also handy to use something like firebug in combination with tiletools.html to download specific tiles that you can then match-up with your large overlay (Ctrl-Shift-C will let you do this in firebug).