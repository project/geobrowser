  function var_dump(obj) {
     if(typeof obj == "object") {
        return "Type: "+typeof(obj)+((obj.constructor) ? "\nConstructor: "+obj.constructor : "")+"\nValue: " + obj;
     } else {
        return "Type: "+typeof(obj)+"\nValue: "+obj;
     }
  }; //var_dump
  
  function getGetVar(searchStr){
   start = location.search.indexOf(searchStr)+searchStr.length+1;
   end = (location.search.indexOf('&', start) == -1)? location.search.length : location.search.indexOf('&', start);
   return unescape(location.search.substring(start, end));
	}
  
  function set_message(message){
  	if (typeof message=="undefined") document.getElementById("message_center").innerHTML = '';
    else document.getElementById("message_center").innerHTML = message;
  }
  
  function hidediv(id) {
    //safe function to hide an element with a specified id
    if (document.getElementById) { // DOM3 = IE5, NS6
      document.getElementById(id).style.display = 'none';
    }
    else {
      if (document.layers) { // Netscape 4
        document.id.display = 'none';
      }
      else { // IE 4
        document.all.id.style.display = 'none';
      }
    }
    document.getElementById('info_frame').src = geobrowser_path + "/loading.html";
  }; //hidediv
  
  function showdiv(id) {
    //safe function to show an element with a specified id
        
    if (document.getElementById) { // DOM3 = IE5, NS6
      document.getElementById(id).style.display = 'block';
    }
    else {
      if (document.layers) { // Netscape 4
        document.id.display = 'block';
      }
      else { // IE 4
        document.all.id.style.display = 'block';
      }
    }
  }; //showdiv
  
  function framed_adjustments(){
  	if (top.location != location) {
		document.getElementById("geobrowser_title").style.visibility = "hidden";
		document.getElementById("view_fullscreen").style.display = "block";
		hidediv('info_window');
	}
  }
  function start_map_move(){
    start_lat_min = map.getBounds().getSouthWest().lat();
    start_lat_max = map.getBounds().getNorthEast().lat();
    start_lon_min = map.getBounds().getSouthWest().lng();
    start_lon_max = map.getBounds().getNorthEast().lng();
  }; //start_map_move
  
  function end_map_move(){
    dump_outside_markers();
    if ((map.getBounds().getNorthWest().lat > start_lat_max) && (map.getBounds().getNorthWest().lng > start_lon_min)) {
      load_markers(map.getBounds().getNorthWest().lat, start_lat_max, map.getBounds().getNorthEast().lng, map.getBounds().getNorthWest().lng);
      load_markers(start_lat_max, map.getBounds().getSouthWest().lat, map.getBounds().getNorthEast().lng, start_lat_max);
      }
  }; //end_map_move
  
  
  function initial_loader(){
    j=0;
    while (j<tree.length) {
      var layers_string = tree[j].getAllChecked(); //Get all layers. We use the getAllChecked command, which is regretable, as there is no getAll function
      var all_layers = layers_string.split(','); 
      for (var i=0; i<all_layers.length; i++) { //Load up all our Icons.
        var tid = all_layers[i];
        marker_icons[tid] = new GIcon();
        marker_icons[tid].image = tree[j].imPath + tree[j].getItemImage(tid, 0) ;
        marker_icons[tid].iconAnchor = new GPoint((icon_width/2), (icon_height/2));
        marker_icons[tid].iconSize = new GSize(icon_width, icon_height);
      }
      j++;
    }
     certain_cats();
     change_tree(initial_tree);
    
  }; //initial_loader
  
  function go_info_frame(url_loc){
  	document.getElementById('info_frame').src = url_loc;
		showdiv('info_window');
  }
  
  function node_clicked(marker, point){
  	//try{
  		if (marker instanceof GMarker){
  			
  			if (document.location == top.location) info_frame_links = "true";
  			else info_frame_links = "false";
  			
  			//Figure out the size of the info window
  			client_width = document.documentElement.clientWidth;
  			client_height = document.documentElement.clientHeight;
  			if ((client_width/2.6) < 250) info_width = 250;
  			else info_width = Math.round(client_width/2.6);
  			if ((client_height/2) < 200) info_height = 200;
  			else info_height = Math.round(client_height/2);
  			
  			//Figure out if we need to move the map to properly display the window, and if we do, then move it!
  			sidebar_width =  Math.round(client_width * 0.25);
  			pop_up_width = info_width + 32;
  			pop_up_offset = (pop_up_width - 95)/2;
  			pop_up_height = 70 + 32 + info_height;
  			left_lng = map.getBounds().getSouthWest().lng();
  			right_lng = map.getBounds().getNorthEast().lng();
  			top_lat = map.getBounds().getNorthEast().lat();
  			bottom_lat = map.getBounds().getSouthWest().lat();
  			percent_left = (marker.getLatLng().lng() - left_lng) / (right_lng - left_lng);
  			pixel_left = Math.round(percent_left * client_width);
  			pop_up_right_from_left = pixel_left + pop_up_offset + 95;
  			pop_up_left_from_left = pixel_left - pop_up_offset;
  			sidebar_from_left = client_width - sidebar_width;
  			pop_side_diff = pop_up_right_from_left - sidebar_from_left;
				if (pop_side_diff > 0){
					lng_width = right_lng - left_lng;
					lat_height = top_lat - bottom_lat;
					pixels_per_lng = client_width / lng_width;
					pixels_ler_lat = client_height / lat_height;
					current_cen = map.getCenter();
					lat_diff = top_lat - marker.getLatLng().lat();
					pixel_lat_diff = Math.round((lat_diff * pixels_ler_lat) - pop_up_height - 1);
					new_lng = (pop_side_diff/pixels_per_lng) + current_cen.lng();
					new_loc = new GLatLng(current_cen.lat(), new_lng);

					if (pixel_lat_diff > 0) map.panTo(new_loc);
					else map.setCenter(new_loc);
				}
				  			
  			map.openInfoWindowHtml(marker.getLatLng(), "<iframe name='node_info_name' id='node_info' style='width:" + info_width + "px; height:" + info_height + "px; overflow-x: hidden;' scrolling='auto' src='?q=geobrowser_view_node/" + marker.nid + "/" + info_frame_links + "'>");

  		}
  		else{
  			if (new_marker_mode){
  				document.getElementById('info_frame').src = base_path + "?q=node/add/" + new_marker_node_type;
	    		showdiv('info_window');
	    		set_info_window_lat_lon(point.lat(), point.lng(), 0);
  			}
  		}
  		new_marker_mode = false;
	    set_message();
	  //}catch(err){}
  }; //node_clicked
  

  
  function set_info_window_lat_lon(lat, lon,count){
  	try{
  		frames['info_frame_name'].document.getElementById('edit-locations-0-locpick-user-latitude').value = lat;
  		frames['info_frame_name'].document.getElementById('edit-locations-0-locpick-user-longitude').value = lon;
  	}
  	catch(err){
  		if (count <= 20){
  			count = count + 1;
  			setTimeout("set_info_window_lat_lon("+lat+","+lon+","+count+")",500);
  		}
  	}
  }
  
  function map_redraw(){
    map.clearOverlays(); // clear the markers off the screen
    marker_set.length = 0; //reset the marker array
    map_load_markers(); //reload the markers
  }; //map_redraw
  
  function map_zoomed(oldLevel, newLevel){
    if (oldLevel < newLevel) map_load_markers(); else map_redraw();
  }; //map_zoomed
  
  function catagory_clicked(clicked_id){
	  uncheck_tree();
    tree[current_tree].setSubChecked(clicked_id,1);
    map_redraw();
  }; //catagory_clicked
  
  function check_categories(checked_cats_csv, tree_to_check){
  	var tree_to_check = (tree_to_check == null) ? current_tree : tree_to_check;
  	uncheck_tree(tree_to_check);
  	var checked_cats = checked_cats_csv.split(',');
  	for (var i=0; i<checked_cats.length; i++) {
      tree[tree_to_check].setSubChecked(checked_cats[i],1);
    }
  }
  
  function uncheck_tree(tree_to_uncheck) {
  	var tree_to_uncheck = (tree_to_uncheck == null) ? current_tree : tree_to_uncheck;
  	var layers_string = tree[tree_to_uncheck].getAllChecked();
    var all_layers = layers_string.split(','); 
    for (var i=0; i<all_layers.length; i++) {
      tree[tree_to_uncheck].setCheck(all_layers[i],0);
    }
  }
  
  function get_layer_string(){
  	var layers_string = "";
    j=0;
    while(j<tree.length) {
    	var each_layer_string = tree[j].getAllChecked();
      if (each_layer_string == '') each_layer_string = "0";
      layers_string += "&layer" + j + "=";
      layers_string += each_layer_string;
      j++;
    }
  	
  	return layers_string;
  	
  }
  
  function map_load_markers(){ // map_load_markers is called whenever we move the map or we click on or off some layers on the tree. Via a GXmlHttp request we get a list of points that match our layers and fit within our map window.
    
    var lat_min = map.getBounds().getSouthWest().lat();
    var lat_max = map.getBounds().getNorthEast().lat();
    var lon_min = map.getBounds().getSouthWest().lng();
    var lon_max = map.getBounds().getNorthEast().lng();
    
    var url_to_open = base_path + "?q=geobrowser_xml_controller&function=map_node" + get_layer_string() + "&nid=" + map_nid + "&current_tree=" + current_tree +"&lat_min=" + lat_min + "&lat_max=" + lat_max + "&lon_min=" + lon_min + "&lon_max=" + lon_max;
    
    function remove_markers(element, keyVar, array) { //A function for clearing all the markers outside our view
          if ((marker_set[keyVar].getPoint().lat() > lat_max) || (marker_set[keyVar].getPoint().lat() < lat_min) || (marker_set[keyVar].getPoint().lng() > lon_max) || (marker_set[keyVar].getPoint().lng() < lon_min)) {
          map.removeOverlay(marker_set[keyVar]);
          delete marker_set[keyVar];
          }
    }
  
    //Javascript 1.5 implements forEach, which is faster. So we try that first. If the user does not have 1.5 then we try the older, slower method
    try {
    	marker_set.forEach(remove_markers)
    } catch(error) {
    	for (i in marker_set) {
    		remove_markers(marker_set[i], i, marker_set);
    	}
    }
    
    GDownloadUrl(url_to_open, function(data, responseCode){
      var xml = GXml.parse(data);
      var new_markers = xml.documentElement.getElementsByTagName("marker");
      for (var y = 0; y < new_markers.length; y++) { //for each marker found in the xml file
        
        //get the nid from the xml
        var nid = new_markers[y].getAttribute("nid");
  
        if (!marker_set[nid]) { //if marker does not yet exist, then load it up!
  
          //grab the lat and long info and parse into a format google maps can understand
          var point = new GLatLng(parseFloat(new_markers[y].getAttribute("lat")),parseFloat(new_markers[y].getAttribute("lng")));
          
          //grab the rest of the attributes and load into variables
          var name = new_markers[y].getAttribute("name");
          var layer = new_markers[y].getAttribute("layer");
  
          //Create the GMarker and load the variables into it
          marker_set[nid] = new GMarker(point, {icon: marker_icons[layer], draggable: draggable_markers});
          marker_set[nid].nid = nid;
          marker_set[nid].name = name;
          marker_set[nid].layer = layer;
          
          setupMarkerForTooltip(map, marker_set[nid], marker_set[nid].name ,nid); // turn on tooltip support 
          setShowTooltipOnMouseover(marker_set[nid]);
          map.addOverlay(marker_set[nid]);
          
          if (draggable_markers){
	          marker_set[nid].enableDragging();
						GEvent.addListener(marker_set[nid], "dragend", marker_dragged);
					}
          
        }
      }
    });
  }; //map_load_markers
  
  
  function build_tree(i){
    tree[i]=new dhtmlXTreeObject('tree_menu' + i,"100%","100%",0);
    tree[i].enableCheckBoxes(true);
    tree[i].enableThreeStateCheckboxes(true);
    tree[i].imPath = base_path + files_path + "/";
    
    tree[i].setOnClickHandler(function Clicking(clicked_id){catagory_clicked(clicked_id);});
    tree[i].setOnCheckHandler(function(){if (map) {map_redraw()}});
    
    try {
    tree[i].loadXML(base_path + "?q=geobrowser_xml_controller&function=build_tree&vid=" + vocabs[i], function (){
        if (i == (vocabs.length -1)) {
        	setTimeout("initial_loader()",500);
        }
      });//load root level from xml
    } catch(e) {
    	alert(e)
    }
      
    //Setting up the titles and tabs.
    if (vocabs.length >= 1){
    	var vocab_title = vocab_titles[vocabs[i]];
    	document.getElementById("tabnav").innerHTML += "<li><a id='tab" + i + "' href='#' onClick='change_tree(" + i + ");'><span>" + vocab_title + "</span></a></li>";
  	}
  }; //build_tree
  
  
  function change_tree(i){
    var j = 0;
    while (j < tree.length) {
      document.getElementById('tree_menu' + j).style.display = 'none';
      document.getElementById('tab' + j).style.top = '0px';
      document.getElementById('tab' + j).style.borderBottom = '1px solid black';
      j++;
    }
    document.getElementById('tree_menu' + i).style.display = 'block';
    document.getElementById('tab' + i).style.top = '1px';
    document.getElementById('tab' + i).style.borderBottom = 'none';
    
    current_tree = i;
    map_redraw();
  }; //change_tree
  
  
  function marker_dragged() {
				   document.getElementById('info_frame').src = base_path + "?q=node/" + this.nid + "/edit";
				   showdiv('info_window');
				   var latlon = this.getPoint();
					 set_info_window_lat_lon(latlon.lat(), latlon.lng(), 0);
  }
  
  function new_marker(nodetype){
  	new_marker_mode = true;
  	set_message(add_marker_text);
  	new_marker_node_type = nodetype;
  }